from django.utils import simplejson
from license.models import License
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def register_license(request):
    email = request.POST.get('email', '')
    license_n = request.POST.get('license', '')
    secret = request.POST.get('secret', '')
    count = int(request.POST.get('count', 0))
    item = {}
    try:
        license = License.objects.get(email=email,license=license_n)
    
        #Is license inactive
        if license.status == 'INACTIVE':
            item['valid'] = False
            item['message'] = 'License is inactive'
            return HttpResponse(simplejson.dumps(item))
        #Is used but valid
        #if license.status == 'USED':
        #    if secret != license.secret:
        #        item['valid'] = False
        #        item['message'] = 'License is in use'
        #        return HttpResponse(simplejson.dumps(item))
        #Still have installs
        if license.number - int(count) < -1:
            license.install_count = license.install_count + 1
            license.number = 0
            license.save()
            item['valid'] = False
            item['message'] = 'You dont have install left'
            return HttpResponse(simplejson.dumps(item))

        #license.status = 'USED'
        license.install_count = license.install_count + count
        license.number = license.number - count
        license.save()
        item['valid'] = True
        item['secret'] = license.secret
        item['left'] = license.number 
        return HttpResponse(simplejson.dumps(item))
    except License.DoesNotExist:
        item['valid'] = False
	item['message'] = "Invalid license"
    return HttpResponse(simplejson.dumps(item))
