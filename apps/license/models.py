# -*- coding: utf-8 -*-

from django.db import models
from license import strings
from uuid import uuid4

class License(models.Model):
    
    def generate_license():
        random_string_length = 24
        return str(uuid4()).replace('-', '')[:random_string_length]


    STATUS_CHOICES = (
        ('ACTIVE',   'Active'),
        ('USED'  ,   'Used'),
        ('INACTIVE', 'Inactive'),
    )

    email = models.EmailField(strings.EMAIL, max_length=128)
    license = models.CharField(
        strings.LICENSE, 
        max_length=128,
        default=generate_license
    )
    days = models.IntegerField(strings.DAYS,default=3)
    number = models.IntegerField(strings.NUMBER,default=5)
    status = models.CharField(
        strings.STATUS,
        choices=STATUS_CHOICES,
        max_length=128,
        default='ACTIVE'
    )
    installed_date = models.DateField(strings.DATE, blank=True, null=True)
    install_count = models.IntegerField(strings.INSTALL_COUNT,default=0)
    secret = models.CharField(
        strings.LICENSE,
        max_length=128,
        default=generate_license
    )

    def __unicode__(self):
        return self.email
