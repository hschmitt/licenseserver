# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns, include, url
from license import views

urlpatterns = patterns('',
    url(r'^license.json$',
        views.register_license,
        name='register_license',
    ),
)
