# -*- coding: utf-8 -*-

from django.contrib import admin
from license.models import License


class LicenseAdmin(admin.ModelAdmin):
    exclude = ('install_count',)
    list_display = ('email', 'license', 'number')

admin.site.register(License, LicenseAdmin)
