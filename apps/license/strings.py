# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _

EMAIL   = _(u'Email')
ACTIVE  = _(u'Active')
USED    = _(u'Used')
LICENSE = _(u'License')
DAYS    = _(u'Days')
DATE    = _(u'Installed Date')
INSTALL_COUNT = _('Install Count')
NUMBER  = _(u'Remaining Licenses')
STATUS  = _(u'Status')

