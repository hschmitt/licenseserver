# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'License'
        db.create_table(u'license_license', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=128)),
            ('license', self.gf('django.db.models.fields.CharField')(default='2820d00f742c4b78898ec7a6', max_length=128)),
            ('days', self.gf('django.db.models.fields.IntegerField')(default=3)),
            ('number', self.gf('django.db.models.fields.IntegerField')(default=5)),
            ('status', self.gf('django.db.models.fields.CharField')(default='ACTIVE', max_length=128)),
            ('installed_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('install_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('secret', self.gf('django.db.models.fields.CharField')(default='2992ce1280534ad1a646fbc2', max_length=128)),
        ))
        db.send_create_signal(u'license', ['License'])


    def backwards(self, orm):
        # Deleting model 'License'
        db.delete_table(u'license_license')


    models = {
        u'license.license': {
            'Meta': {'object_name': 'License'},
            'days': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'install_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'installed_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'license': ('django.db.models.fields.CharField', [], {'default': "'1f77871627964964bd05bdc8'", 'max_length': '128'}),
            'number': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'secret': ('django.db.models.fields.CharField', [], {'default': "'ddd84e05dd4a4c0a9ac86dd8'", 'max_length': '128'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'ACTIVE'", 'max_length': '128'})
        }
    }

    complete_apps = ['license']